#title Perché la bandiera nera?
#author Emma Goldman
#LISTtitle Perché la bandiera nera?
#SORTauthors Goldman, Emma;
#SORTtopics anarchismo
#lang it
#pubdate 2018-04-25T08:00:00


La bandiera nera è il simbolo dell’Anarchia. Essa provoca reazioni che vanno dall’orrore alla delizia tra quelli che la riconoscono. Cercate di capire cosa significa e preparatevi a vederla sempre più spesso in pubblico. Gli Anarchici sono contro tutti i governi perché credono che la libera ed informata volontà dell’individuo sia la vera forza dei gruppi e della stessa società.

Gli Anarchici credono nell’iniziativa e nella responsabilità individuali e nella completa cooperazione dei gruppi composti di liberi individui. I governi sono l’opposto di questi ideali, dato che si fondano sulla forza bruta e la frode deliberata per imporre il controllo dei pochi sui molti. Che questo processo crudele e fraudolento sia giustificato da concetti come il diritto divino, elezioni democratiche, o un governo rivoluzionario del popolo conta poco per gli Anarchici. Noi rigettiamo l’intero concetto stesso di governo e ci affidiamo in modo radicale alla capacità di risoluzione dei problemi propria di ogni uomo libero.

Perché la bandiera nera? Il nero è il colore della negazione. La bandiera nera è la negazione di tutte le bandiere. È la negazione dell’idea di nazione che mette la razza umana contro se stessa e nega l’unità di tutta l’umanità. Il colore nero è il colore del sentimento di rabbia e indignazione nei confronti di tutti i crimini compiuti nel nome dell’appartenenza allo stato. È la rabbia e l’indignazione contro l’insulto all’intelligenza umana insito nelle pretese, ipocrisie e bassi sotterfugi dei governi.

Il nero è anche il colore del lutto; la bandiera nera che cancella le nazioni è anche simbolo di lutto per le loro vittime, i milioni assassinati nelle guerre, esterne ed interne, a maggior gloria e stabilità di qualche maledetto stato. È a lutto per quei milioni il cui lavoro è derubato (tassato) per pagare le stragi e l’oppressione di altri esseri umani. È a lutto non solo per la morte del corpo, ma anche per l’annullamento dello spirito sotto sistemi autoritari e gerarchici. È a lutto per i milioni di cellule grigie spente senza dar loro la possibilità di illuminare il mondo. È il colore di una tristezza inconsolabile.

Ma il nero è anche meraviglioso. È il colore della determinazione, della risoluzione, della forza, un colore che definisce e chiarifica tutti gli altri. Il colore nero è il mistero che circonda la germinazione, la fertilità, il suolo fertile che nutre nuova vita che continuamente si evolve, rinnova, rinfresca, e si riproduce nel buio. Il seme nascosto nella terra, lo strano viaggio dello sperma, la crescita segreta dell’embrione nel grembo materno – il colore nero circonda e protegge tutte queste cose.

Così il colore nero è negazione, rabbia, indignazione, lutto, bellezza, speranza, è il nutrimento e il riparo per nuove forme di vita e di relazioni sulla e con la terra. La bandiera nera significa tutte queste cose. Noi siamo orgogliosi di portarla, addolorati di doverlo fare, e speriamo nel giorno nel quale questo simbolo non sarà più necessario.
