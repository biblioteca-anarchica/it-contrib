#title Spiriti liberi
#author Anonimo
#SORTtopics Autonomia Operaia, neo-operaismo, operaismo, Comitato Invisibile, Tronti, Tarì, critica
#date 25 marzo 2018
#source [[https://finimondo.org/node/2141][finimondo.org]] consultato nei giorni della Santa Pasqua 2018
#lang it
#pubdate 2018-03-31T14:30:11
#notes Un breve commento alla kermesse <em>Qui e Ora Experience</em> ed al dibattito con il senatore del PD e «marxista ratzingeriano» Mario Tronti



«Vi dico che non sarei qui se non fossi partito da lì, qui a fare politica per gli stessi fini con altri mezzi; è un esercizio addirittura spericolato, ma entusiasmante, se entusiasmo può esserci ancora concesso in questi tristi tempi.

Vi chiedo ancora scusa».

                                         <em>Mario Tronti, 2017</em>

<br>

Con queste parole con cui ha giustificato il proprio voto in Senato a favore della legge elettorale nota come <em>Rosatellum</em>, il grande vecchio dell’operaismo ha dimostrato di essere anche il grande vecchio degli spiriti liberi da identitarismi radicali, degli oltrepassatori di ideologie ammuffite, dei ribelli immuni da pregiudizi paralizzanti, dei lucidi strateghi di alleanze necessarie (ovvero di coloro che noialtri, resi impotenti dalle nostre passioni tristi, chiamiamo voltagabbana). Gente per cui la differenza fra il mucchio selvaggio della rivolta e la grande ammucchiata della politica è solo di circostanza, di occasione, di calcolo. Oggi si infrangono vetrine come domani si presentano petizioni. Stessi fini, altri mezzi?

Per scoprirlo si potrebbe andare i prossimi 6 e 7 aprile al Teatro Tordinona di Roma, dove verrà rappresentato lo spettacolo rivoluzionario <em>Qui e Ora Experience</em>. Ad organizzarlo, alcuni italici esponenti del Fan Club del Comitato Invisibile. Non la sezione dei loro sottopancia libertari, no, l’altra, la loro sponda autoritaria.

Performance artistiche a parte, gli appuntamenti più appetitosi dell’iniziativa saranno i dibattiti. Il primo giorno l’ex maestro di Toni Negri dibatterà con un ex allievo di Toni Negri sull’esperienza del comunismo (ehm... la dissociazione?). Non potendo avere di nuovo l’illuminante Maria Elena Boschi come spalla, né lo sbirro Minniti come garante, il senatore del PD Mario Tronti si dovrà accontentare questa volta del cupo Marcello Tarì per discettare su un’autonomia che non ci pare per nulla diffusa, ma unicamente immaginaria ed invisibile. Un po’ come quella teologia politica cui il vecchio «marxista ratzingeriano» (secondo cui il cristianesimo è «la tradizione che sta all’origine dell’idea stessa di libertà») e il giovane blanquista agambeniano (per cui «una guerra di religione, deve essere combattuta qui e ora dai rivoluzionari») potranno elevare in gaudio e letizia lodi al cielo della metafisica.

Il secondo giorno invece comincia facendo l’occhiolino all’ultima fatica del Comitato Invisibile (i cui sostenitori di Rennes hanno <em>Adesso</em> dato prova della loro esperienza “insurrezionale” del presente sostituendosi alla polizia e sgomberando gli ostinati irriducibili della Zad che non volevano rassegnarsi al ritorno all’ordine) e prosegue con la rievocazione del maggio 68 con uno dei fondatori e leader di Potere Operaio (no, non Scalzone, Piperno). A vegliare sull’intera due giorni, gli immancabili tenutari della Calusca di Milano con una mostra sul 68 e dintorni.

In questi tristi tempi, una <em>Experience</em> entusiasmante e spericolata. Vi abbiamo fatto venir gola, eh?

<br>



