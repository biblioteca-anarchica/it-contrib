#title Nichilismo
#subtitle Anno I, n.4, 21 maggio 1920
#author Renzo Novatore
#SORTauthor Novatore, Renzo;
#LISTtitle Nichilismo N.4
#SORTtopics nichilismo
#date 1920
#source Consultato il giorno 20/07/2019 su [[https://simonettiwalter.blogspot.com/2012/11/renzo-novatore-nichilismo-anno-i-n-4-21.html][simonettiwalter.blogspot.com]]
#lang it
#nocoverpage 1
#pubdate 2021-11-04T18:00:53


**** I

Sono individualista perché anarchico, e sono anarchico perché sono nichilista. Ma anche il nichilismo lo intendo a modo mio…

Non mi occupo di sapere se esso sia nordico od orientale, né se abbia o non abbia una tradizione storica, politica, pratica o teorica, filosofica, spirituale od intellettuale. Mi dico nichilista solo perché so che nichilismo vuol dire negazione! Negazione di ogni società, di ogni culto, di ogni regola e di ogni religione. Ma non agogno al Nirvana come non anelo al pessimismo disperato ed impotente dello Schopenhauer, che è qualche cosa di peggio della stessa rinnegazione violenta della vita. Il mio, è un pessimismo entusiasta e dionisiaco come le fiamme che incendiano la mia esuberanza vitale, che irride a qualsiasi prigione teoretica, scientifica e morale.

E se mi dico anarchico individualista, iconoclasta e nichilista, è appunto perché credo che in questi aggettivi siavi l’espressione massima e completa della mia volitiva e scapigliata individualità, che, come un fiume straripante, vuole espandersi impetuosamente travolgendo argini e siepi, fintanto che, urtando in un granitico masso, s’infranga e si disperda a sua volta. Io non rinnego la vita. La sublimo e la canto.

**** II

Chi rinnega la vita perché crede che questa non sia che Male e Dolore e non trova in se stesso l’eroico coraggio dell’autosoppressione è — per me — un grottesco posatore, un impotente; come è un essere compassionevolmente inferiore colui che crede che l’albero santo della felicità sia una pianta contorta sulla quale tutte le scimmie possono arrampicarsi in un più o meno prossimo avvenire, e che allora la tenebra del male sarà fugata dai razzi fosforescenti del vero Bene…

**** III

La vita — per me — non è né un bene né un male, né una teoria né un’idea. La vita è una realtà, e la realtà della vita è la guerra. Per chi è nato guerriero la vita è una sorgente di gioia, per gli altri non è che una sorgente di umiliazione e di dolore. Io non chiedo più alla vita la gioia spensierata. Essa non potrebbe darmela ed io non saprei più che farmene ormai che l’adolescenza è passata…

Le chiedo invece la gioia perversa delle battaglie che mi danno i fremiti dolorosi delle sconfitte ed i voluttuosi brividi delle vittorie.

Vinto sul fango o vittorioso nel sole, io canto la vita e l’amo!

Per l’anima mia ribelle non vi è pace che nella guerra, come, per il mio spirito vagabondo e negatore, non vi è felicità più grande della spregiudicata affermazione della mia capacità di vivere e di tripudiare. Ogni mia sconfitta mi serve soltanto come preludio sinfonico ad una nuova vittoria.

**** IV

Dal giorno ch’io venni alla luce — per una casuale combinazione che non mi importa ora di approfondire — portai con me il mio Bene ed il mio Male.

Vale a dire: la mia gioia e il mio dolore ancora in embrione. L’uno e l’altro progredirono con me nel cammino del tempo. Quanto più intensa ho provata la gioia tanto più profondo ho inteso il dolore.

Ma questo non può essere soppresso senza la soppressione di quello.

Ora ho scardinato la porta del mistero ed ho sciolto l’enigma della Sfinge. La gioia ed il dolore sono i due soli liquori componenti la bevanda eroica colla quale si ubriaca allegramente la vita. Perché non è vero che questa sia uno squallido e pauroso deserto ove non germina più nessun fiore né più matura nessun frutto vermiglio.

Ed anche il più possente di tutti i dolori, quello che sospinge il forte verso lo sfasciamento cosciente e tragico della propria individualità, non è che una vigorosa manifestazione d’arte e di bellezza.

Ed anch’esso rientra nella corrente universale dell’umano pensiero coi raggi folgoreggianti del crimine che scardina e travolge ogni cristallizzata realtà del circoscritto mondo dei più per ascendere verso l’ultima fiamma ideale e disperdersi nel sempiterno fuoco del nuovo.

**** V

La rivolta dell’uomo libero contro il dolore non è che l’intimo passionale desiderio d’una gioia più intensa e più grande. Ma la gioia più grande non sa mostrarsi all’uomo che nello specchio del più profondo dolore, per poscia fondersi con questo in un enorme e barbaro amplesso. Ed è da questo enorme e fecondo amplesso che scaturisce il superiore e saettante sorriso del forte, che attraverso la lotta canta l’inno più scrosciante alla vita.

Inno intessuto di disprezzo e di scherno, di volontà e di potenza. Inno che vibra e palpita fra la luce del sole che irradia le tombe; inno che rianima il nulla e lo riempie di suoni.

**** VI

Sopra lo spirito schiavo di Socrate che accetta stoicamente la morte e lo spirito libero di Diogene che accetta cinicamente la vita, si erge l’arco trionfale sul quale danza il sacrilego frantumatore de’ nuovi fantasmi, il radicale distruttore di ogni mondo morale. È l’uomo libero che danza in alto, fra le magnifiche fosforescenze del sole.

E quando si alzano dai paludosi abissi le gigantesche nubi gonfie di cupa tenebra per impedirci la vista della luce ed ostacolarci il cammino, egli si apre il varco a colpi di Browning o ferma il loro corso colla fiamma del suo pensiero e della sua fantasia dominatrice, imponendo loro di soggiacere come umili schiave ai suoi piedi.

Ma solo chi conosce e pratica i furori iconoclastici della distruzione può possedere la gioia nata dalla libertà, di quella unica libertà fecondata dal dolore. Io mi ergo contro la realtà del mondo esteriore per il trionfo della realtà del mio mondo interiore.

Nego la società per il trionfo dell’io. Nego la stabilità di ogni regola, di ogni costume, di ogni morale, per l’affermazione di ogni istinto volitivo, di ogni libera sentimentalità, di ogni passione e di ogni fantasia. Irrido ad ogni dovere ad ogni diritto per cantare il libero arbitrio.

Schernisco l’avvenire per soffrire e godere nel presente il mio bene ed il mio male. L’umanità la disprezzo perché non è la mia umanità. Odio i tiranni e detesto gli schiavi. Non voglio e non concedo solidarietà perché credo che sia una nuova catena, e perché credo con Ibsen che l’uomo più solo è l’uomo più forte.

Questo è il mio Nichilismo. La vita, per me, non è che un eroico poema di gioia e di perversità scritto dalle mani sanguinanti del dolore e del male o un sogno tragico d’arte e di bellezza!



