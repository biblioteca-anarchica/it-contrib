#title L’apparenza della pratica
#author Machete (rivista)
#LISTtitle apparenza della pratica
#SORTauthors Machete (rivista)
#SORTtopics scienza, civilizzazione, Machete n. 2
#date aprile 2008
#lang it
#pubdate 2018-03-27T23:14:24
#source Consultato il 26 marzo 2018 su [[http://digidownload.libero.it/guerrasociale.org/machete2.pdf][digidownload.libero.it]]


L’uomo cerca risposte efficaci agli interrogativi che lo attanagliano. Pensa di solito in termini di utilità pratica ogni problema che gli si pone. Considera efficace una risposta quando ne può trarre un’utilità per la vita pratica, quando la sua attività funzionale produce oggetti pratici per l’uso quotidiano. L’uomo sottopone il suo pensiero ad uno sforzo incessante nella ricerca di soluzioni utilizzabili praticamente. Non si accontenta delle soluzioni del passato e aspetta dal futuro il superamento delle imperfezioni presenti o trascorse.

Il pensiero, nella sua ricerca febbrile, rischiara come un lampo l’oscurità del futuro e apre agli uomini le possibilità di soluzione ch’esso contiene. O meglio, fornisce loro per un attimo la chiave.

Ma in ogni fulminea illuminazione la chiave può essere afferrata soltanto per poco. Chi l’afferra procede, chi non l’afferra rimane fermo sulle vecchie posizioni. Ma chi ha potuto afferrare la chiave avrà realmente raggiunto la compiutezza? Quali vantaggi ha nei confronti di chi si serve ancora della vecchia chiave e crede ai vecchi argomenti, dal momento che entrambi saranno superati se domani una nuova chiave venisse scoperta?

Immersi nella soluzione di problemi pratici, non avvertiamo l’altra domanda: può l’uomo raggiungere la compiutezza cui aspira se mira sempre e soltanto a fini di utilità pratica? Comunque, il passato non ha ancora offerto la prova che su questa via ci si sia avvicinati alla compiutezza pratica. Dimostra semmai che l’uomo non ha ancora raggiunto lo scopo che si è prefisso. Ogni sforzo verso la compiutezza pratica, verso l’oggetto pratico compiuto, naufraga davanti alla verità o realtà dell’essere, che non è oggettivo.

È ragionevole supporre che l’umanità potrebbe pervenire in modo più rapido e sicuro ai risultati pratici desiderati se fosse unita in un’unica società. Invece ognuno tenta continuamente di raggiungere con le proprie forze il fine pratico che gli appare desiderabile.

L’uomo possiede un mezzo: il movimento. Il suo compito più urgente gli sembra quello di fare in modo che nel mondo pratico ci sia un’equilibrata ripartizione del movimento delle masse. Solo così le masse possono progredire uniformemente e solo allora si mantengono in equilibrio pratico. In ciò consiste il più alto obiettivo di ogni riflessione pratico-economica, che deve trovare la sua espressione nell’equilibrio generale. Non appena però qualcuno si fa avanti ed esce dalla massa, comincia a dividerla, oppure se la trascina dietro.

Il mondo dell’oggettività pratica mira a raggiungere una totalità pratica di tipo unitario. A tale scopo ha indirizzato il suo sforzo esclusivamente verso soluzioni pratiche, dividendosi in una molteplicità di occupazioni e cedendo a ognuna di esse una parte del peso totale.

Il pensiero, che ha inventato il mondo dell’oggettività pratica, avrebbe potuto raggiungere altrettanto bene una rappresentazione non-oggettiva se avesse abbandonato questa caccia insensata alle raffigurazioni della fantasia. Creando l’idea di utilità pratica ha però dimostrato d’essere incapace di raggiungere la compiutezza finale della rappresentazione pratica. Il pensiero si fonda sull’utilità pratica, dunque sopra gli errori del passato, e vuole correggerli costantemente. Ciò non impedisce tuttavia che in futuro queste correzioni possano apparire a loro volta errate. Cionondimeno il pensiero è convinto che nel “futuro” si celi la panacea universale. Esso si attende la liberazione dalle sue manchevolezze e un aiuto a raggiungere quella perfezione del mondo pratico a cui non è pervenuto in passato. L’uomo si aspetta dal futuro la rettifica degli errori del passato. Si può però dire con sicurezza che tali correzioni non condurranno alla perfezione dell’essere pratico. L’essere come idea pratica guida la nostra coscienza da una incompiutezza all’altra. La rappresentazione dell’essere è sempre solo una <em>propria</em> rappresentazione; ma la vera essenza rimane sconosciuta. Così, non ci si può aspettare dal futuro nessuna compiutezza pratica. Tutte le conquiste pratico-scientifiche sono solo apparenti. Tutto ciò che è pratico rivela l’insufficienza di ieri. La sfera pratico-utilitaria si perfeziona quotidianamente, ma tutti i miglioramenti toccano solo l’involucro, mentre il nocciolo rimane inadeguato. La civiltà fondata sulla sola pratica è costruita su una logica molto strana: accumulazione di valori da un lato e loro distruzione dall’altro. Così l’essere pratico governa la coscienza dell’uomo. Nessun oggetto può salvarsi dall’effetto prodotto da questa strana logica. L’uomo non può condurre i suoi pensieri fino in fondo. Perciò le cose pratiche, insufficientemente pensate, vengono annientate.

Dalle affermazioni precedenti si può dedurre che l’uomo, nella sua essenza, non è un essere pratico né mai completamente razionale. L’intera civiltà, costituita dalle conquiste pratico-tecniche, è tanto manchevole quanto l’intelletto che la governa. Nessuna cosa o oggetto possono mai essere portati alla compiutezza pratica. Non ci fu, non c’è, né mai ci sarà un oggetto perfetto.

La tecnica scientifica ha scelto la via dell’intelletto per raggiungere risultati logicamente pensati. Ma non riuscirà a centrare il suo bersaglio perché nulla si lascia pensare compiutamente: nessun oggetto ha limiti chiaramente conoscibili al cui interno sia possibile pensarlo a fondo.

Essere pratici significa poter prevedere le cose. Ma allora ogni previsione può essere solo una rappresentazione di avvenimenti possibili o un loro calcolo teorico, e non ci può essere una previsione autentica e precisa. La realtà è inafferrabile. Ora, poiché un fatto “non-oggettivo” non è ammissibile dal punto di vista “logico-scientifico” o “politico”, lo Stato e la società hanno valorizzato praticamente e utilitaristicamente soltanto le caratteristiche esteriori della realtà.

La tecnica scientifica e la politica economica hanno dimostrato di essere incapaci di risolvere il problema della civiltà pratico-oggettiva, di rendere compiuta questa civiltà e di realizzarla praticamente. Se, dopo millenni di tentativi all’interno di civiltà pratico-oggettive fondate sul sapere scientifico, si è giunti a un nulla di fatto, perché non fare anche un tentativo sulla base di un piano non-oggettivo? Un piano cioè di azioni non scientifiche e non logiche, al di fuori di ogni questione di credibilità e validità; un piano che rifiuti ogni significato e fondamento di ordine meramente razionale.

Prigioniero dell’idea del realismo pratico, l’uomo vuole formare l’intera natura secondo il proprio progetto ideale. Tutto il realismo pratico, oggettivo, scientificamente fondato, e tutta la sua civiltà sono però un’idea irrealizzabile. Qui sparisce ogni rappresentazione di idealità, di utilità, di compiutezza.



